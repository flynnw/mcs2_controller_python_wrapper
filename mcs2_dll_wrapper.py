import ctypes as c

# load into csv
properties_csv = open('properties.csv','r')
properties_dict = {}

# dictionary where keys are property name
for line in properties_csv:
    line = line.split(',')
    properties_dict[line[0]] =  {"code" : int(line[1],16), # convert hex code to int
                                 "type" : line[2],
                                 "permissions" : line[4]}

# this part must be sloppily hardcoded
# ctl = cdll.LoadLibrary(r"C:\Users\DSG\Desktop\SmarAct\MCS2_SDK\lib64\SmarActCTL")
ctl = c.cdll.LoadLibrary("/usr/lib/libsmaractctl.so")

# this doesn't do anything as far as I can tell
dHandle = c.c_ulong(1)

# defining an error if SA_CTL functions error
class SA_CTL_Error(Exception):
    def __init__(self,function,code):
        super().__init__("{} errored with code {}".format(function,code))

# function to find devices and connect to one if available
def connect():
    
    namebuffer = c.create_string_buffer(63)
    namebuffersize = c.c_ulong(64)

    find_return_value = ctl.SA_CTL_FindDevices(
                            bytes(),                # const char *options (unused)
                            c.byref(namebuffer),    # char *deviceList
                            c.byref(namebuffersize) # size_t *deviceListLen
                            )
    
    if find_return_value > 0:
        raise SA_CTL_Error("SA_CTL_FindDevices",find_return_value)
    
    print("Device found with MCS address: ",namebuffer.value.decode("utf-8"))

    open_return_value = ctl.SA_CTL_Open(
                          c.byref(dHandle),    # SA_CTL_DeviceHandle_t *dHandle
                          c.byref(namebuffer), # const char *locator
                          bytes())             # const char *config (unused)

    if open_return_value > 0:
        raise SA_CTL_Error("SA_CTL_Open",open_return_value)
    
    print("Connection established.")

# function to retrieve any property listed in manual section 4.1 (p.134)
def get_property(prop,channel=0):

    prop = properties_dict[prop]
    
    return_type = prop["type"]
    
    if return_type == "I32":
        return_value = c.c_int32()
        return_size = c.c_int32(1)
        return_code = ctl.SA_CTL_GetProperty_i32(
                        dHandle,               # SA_CTL_DeviceHandle_t dHandle
                        channel,               # int8_t idx
                        prop["code"],          # SA_CTL_PropertyKey_t pkey
                        c.byref(return_value), # int32_t *value
                        c.byref(return_size))  # size_t *ioArraySize
        
        return_value = return_value.value

    elif return_type == "I64":
        return_value = c.c_int64()
        return_size = c.c_int32(1)
        return_code = ctl.SA_CTL_GetProperty_i64(
                        dHandle,               # SA_CTL_DeviceHandle_t dHandle
                        channel,               # int8_t idx
                        prop["code"],          # SA_CTL_PropertyKey_t pkey
                        c.byref(return_value), # int64_t *value
                        c.byref(return_size))  # size_t *ioArraySize
    
        return_value = return_value.value

    elif return_type == "String":
        return_value = c.create_string_buffer(63)
        return_size = c.c_ulong(64)
        return_code = ctl.SA_CTL_GetProperty_s(
                        dHandle,               # SA_CTL_DeviceHandle_t dHandle
                        channel,               # int8_t idx
                        prop["code"],          # SA_CTL_PropertyKey_t pkey
                        c.byref(return_value), # char *value
                        c.byref(return_size))  # size_t *ioArraySize

        return_value = return_value.value.decode("utf-8")
    else:
        raise TypeError("property return type not known") 
    
    if return_code > 0:
        raise SA_CTL_Error("get property",return_code) 
    
    return return_value 

# function to set any property listed in manual section 4.1 (p.134)
def set_property(prop,set_value,channel=0):
    
    prop = properties_dict[prop]

    return_type = prop["type"]
    
    if return_type == "I32":
        set_value = c.c_int32(set_value)
        return_code = ctl.SA_CTL_SetProperty_i32(
                        dHandle,              # SA_CTL_DeviceHandle_t dHandle
                        channel,             # int8_t idx
                        prop["code"],        # SA_CTL_PropertyKey_t pkey
                        set_value)           # int32_t value

    elif return_type == "I64":
        set_value = c.c_int64(set_value)
        return_code = ctl.SA_CTL_SetProperty_i64(
                        dHandle,              # SA_CTL_DeviceHandle_t dHandle
                        channel,             # int8_t idx
                        prop["code"],        # SA_CTL_PropertyKey_t pkey
                        set_value)          # int64_t value
    
    elif return_type == "String":
        set_value = bytes(set_value,"utf-8")
        return_code = ctl.SA_CTL_SetProperty_s(
                        dHandle,              # SA_CTL_DeviceHandle_t dHandle
                        channel,             # int8_t idx
                        prop["code"],        # SA_CTL_PropertyKey_t pkey
                        set_value)           # const char *value

    else:
        raise TypeError("property return type not known")

    if return_code > 0:
        raise SA_CTL_Error("get property",return_code)

    return 0


def calibrate(channel,setting,value):

    # channel is which sensor/motor
    # setting is calibration setting

    # p. 26 in the manual
    # setting 0 defines direction
    # setting 1 Detect Distance Code Inversion
    # setting 2 Advanced Sensor Correction
    # setting 8 Limited Travel Range
    
    # setting is set to value
    # 0 = forward, 1 = backward for direction

    # will calibrate in 0 direction (foreward)
    set_property("Calibration Options",value,channel=setting)

    return_code = ctl.SA_CTL_Calibrate(
                       dHandle, # SA_CTL_DeviceHandle_t dHandle
                       channel, # int8_t idx
                       0)       # SA_CTL_TransmitHandle_t tHandle


    return return_code



def move(channel,mode,value,velocity,acceleration=0,holdtime=-1):
    """
    Valid move modes 

    SA_CTL_MOVE_MODE_CL_ABSOLUTE (0)
    SA_CTL_MOVE_MODE_CL_RELATIVE (1)
    SA_CTL_MOVE_MODE_SCAN_ABSOLUTE(2)
    SA_CTL_MOVE_MODE_SCAN_RELATIVE(3)
    SA_CTL_MOVE_MODE_STEP (4)

    Generally, the base unit for position values is pico meters (pm) for linear positioners and nano degrees (n°) for rotary positioners.

    velocity system is in pm/s, nm/s for us
    acceleration system is pm/s^2, nm/s^2 for us
    value is in pm, nm for us
    hold time is in ms
    """
    set_property("Move Mode",mode) # 0-4
    
    # convert to nm and handle scientific notation
    velocity = int(1000*velocity)
    acceleration = int(1000*acceleration)
    value = int(1000*value)

    set_property("Move Velocity",velocity)
    set_property("Move Acceleration",acceleration)
    set_property("Hold Time",holdtime)

    return_code = ctl.SA_CTL_Move(
                     dHandle, # SA_CTL_DeviceHandle_t dHandle
                     channel, # int8_t idx
                     value,   # int64_t moveValue (interpretation depends on mode)
                     0)        # SA_CTL_TransmitHandle_t tHandle

    if return_code > 0:
        raise SA_CTL_Error("SA_CTL_Move",return_code)


def abs_move(channel,end_position,velocity,acceleration=0,holdtime=-1):
    
    move(channel,0,end_position,velocity,acceleration,holdtime)

def stop():
    return_code = ctl.SA_CTL_Stop(
                      dHandle, # SA_CTL_DeviceHandle_t dHandle,
                      0,     # int8_t idx,
                      0)     # SA_CTL_TransmitHandle_t tHandle

    return return_code

def close():
    return_code = ctl.SA_CTL_Close(dHandle)
    if return_code > 0:
        raise SystemError("Errored in closing?")

# Error compendium
# 61441 device not found in open
# 61447 device not connected
# 34 get propert of channel that doesn't exist
# 259 get property of channel that's not plugged in
